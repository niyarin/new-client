﻿YourFile = {};

(function(global) {
    // 以下の二つの関数を下で定義する。この二つは自動で呼ばれる
    global.YourFile.init = init; // () => void
    global.YourFile.onMessage = onMessage; // (object) => void

    // 以下は自分で生成するオブジェクト
    var keyBoard = null;
    var objectTree = null;

    // 以下のObjectを custom-area1 に表示する
    var display = {
        "キー": "値",
        "多重": {
            "key": "value"
        }
    };

    var state = {

    };

    function init() {
        // q a z w s x を button の index 0 1 2 3 4 5 に対応づけてkeyBoardオブジェクトを作る
        keyBoard = new KeyBoardGamepad(KeyBoardGamepad.strToCharCodes("qazwsx"), []);

        var mycamera = new CameraStreamClient("ws://localhost:8080");
        niyarinClient.cameraViewer.setSingleMode(mycamera);

        objectTree = new ObjectTree(document.getElementById("custom-area1"));
        objectTree.init(display);

        window.setInterval(tasks, 50);
    };

    function onMessage(data) {
        // data: control サーバーからのデータがJSON.parse()されたもの
    }

    function tasks() {
        // init内でsetIntervalすることで周期的に呼ばれるようにしている
        var buttons = niyarinClient.gamePad.getButtons();
        var axes = niyarinClient.gamePad.getAxes();
        var pressedButton = niyarinClient.gamePad.getPressed();
        var key = keyBoard.getButtons();

        niyarinClient.controlClient.send(JSON.stringify(state));
    };

})((this || 0).self || global);