(function(global) {
    // public
    global.TimeClient = function(url) {
        this.ws = new WebSocket('ws://' + url, 'time-server');
        this.on_start = function() {}
        this.on_end = function() {}
        this.on_update = function(left) {}

        this.ws.onmessage = function(ev) {
            var data = JSON.parse(ev.data);
            if (data.start) this.on_start();
            else if (data.end) this.on_end();
            if (data.left) this.on_update(data.left);
        }.bind(this);
    }
    global.TimeClient.prototype.start = function(left) {
        $.post('/time-server/start', {
            time: left === undefined ? 900 : left
        });
    }
})((this || 0).self || global);