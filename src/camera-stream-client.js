/*
    usage :                         
    dependency:                
 */

(function(global) {
    // public
    global.CameraStreamClient = CameraStreamClient; // (url) => object 
    var cc = global.CameraStreamClient;

    cc.prototype.onConnectionChanged = onConnectionChanged;
    cc.prototype.onStatusReceived = onStatusReceived;
    cc.prototype.onImageReceived = onImageReceived;

    // mothod
    cc.prototype.send = send; // (data) => void
    cc.prototype.changeCamera = changeCamera; // (no?) => void , if no is null next number is used
    cc.prototype.getCameraNo = getCameraNo;
    // member variable
    //     isConnected : bool
    //     url : target url
    //     websocket : websocket    

    // private

    function CameraStreamClient(url, cameraCnt) {
        this.websocket = null;
        this.isConnected = false;
        this.url = url;
        this.cameraCnt = cameraCnt !== null ? cameraCnt : 4;
        tryConnect.call(this);
        this.cameraNo = 0;
        this._onConnectionChanged = [];
        this._onImageReceived = [];
        this._onStatusReceived = [];
        return this;
    }

    function onConnectionChanged(f) {
        this._onConnectionChanged.push(f);
    };

    function onStatusReceived(f) {
        this._onStatusReceived.push(f);
    };

    function onImageReceived(f) {
        this._onImageReceived.push(f);
    };

    function tryConnect() {
        var ws = new WebSocket(this.url, "camera-stream");
        ws.onmessage = wsOnMessage.bind(this);
        ws.onopen = wsOnOpen.bind(this);
        ws.onclose = wsOnClose.bind(this);
        this.websocket = ws;
    }

    function wsOnOpen(evt) {
        console.log("camera-stream connect");
        this.isConnected = true;
        this._onConnectionChanged.forEach(function(f) {
            f(true);
        });
    }

    function wsOnMessage(evt) {
        if (typeof evt.data === 'string') {
            var data = JSON.parse(evt.data);
            if("camera" in data) this.cameraNo = data.camera;
            
            this._onStatusReceived.forEach(function(f) {
                f(data);
            });
        } else {
            var url = window.URL.createObjectURL(evt.data);
            this._onImageReceived.forEach(function(f) {
                f(url);
            }); // call event
        }
    }

    function wsOnClose(evt) {
        console.log("server close");
        this.isConnected = false;
        this._onConnectionChanged.forEach(function(f) {
            f(false);
        });
        window.setTimeout(tryConnect.bind(this), 1000);
    }

    function send(obj) {
        if(this.isConnected) {
            this.websocket.send(obj);
        }
    }

    function changeCamera(no) {

        this.cameraNo = no !== null ? no : ((this.cameraNo + 1) % this.cameraCnt);

        this.send(JSON.stringify({
            camera: this.cameraNo
        }));
    }

    function getCameraNo() {
        return this.cameraNo;
    }

})((this || 0).self || global);