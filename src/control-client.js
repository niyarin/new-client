/*
 * 制御通信クライアントライブラリ
 * 
 * dependency:
 *      none
 * usage:
 *      var cc = new ControlClient(url); // new instance
 *      cc.onMessage(function(data) {}); // push callback
 *      cc.onConnectionChange(function(isConnected) {})
 *      cc.send(JSON.stringify({ }));    // send data must be json
 */

(function(global) {
    global.ControlClient = ControlClient;
    global.ControlClient.prototype.connect = connect;
    global.ControlClient.prototype.send = send;
    global.ControlClient.prototype.onMessage = onMessage;
    global.ControlClient.prototype.onConnectionChange = onConnectionChange;

    function ControlClient(url) {
        this.websocket = null;
        this.isConnected = null;

        this._onConnectionChanged = [];
        this._onMessageReceived = [];
        if(url !== null) this.connect(url);
    }

    function connect(url) {
        this.url = url;
        if(this.isConnected) {
            this.ws.close();
        } else {
            tryConnect.call(this);
        }
    }

    function send(data) {
        if (this.isConnected) {
            this.websocket.send(data);
            return true;
        }
        return false;
    }


    function onMessage(f) {
        this._onMessageReceived.push(f);
    }

    function onConnectionChange(f) {
        this._onConnectionChanged.push(f);
    }


    function tryConnect() {
        var ws = new WebSocket(this.url, "control");
        ws.onmessage = wsOnMessage.bind(this);
        ws.onopen = wsOnOpen.bind(this);
        ws.onclose = wsOnClose.bind(this);
        this.websocket = ws;
    }

    function wsOnOpen(evt) {
        console.log("control connect");
        this.isConnected = true;
        this.send(JSON.stringify({
            get_config: true
        }));
        this._onConnectionChanged.forEach(function(f) {
            f(true);
        });
    }

    function wsOnMessage(evt) {
        var data = JSON.parse(evt.data);
        this._onMessageReceived.forEach(function(f) {
            f(data);
        });
    }

    function wsOnClose(evt) {
        console.log("control close");
        this.isConnected = false;
        this._onConnectionChanged.forEach(function(f) {
            f(false);
        });
        window.setTimeout(tryConnect.bind(this), 1000);
    }

})((this || 0).self || global);