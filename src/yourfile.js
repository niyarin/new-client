﻿YourFile = {};

(function(global) {
    global.YourFile.init = init;
    global.YourFile.onMessage = onMessage;

    var gamePad = null;
    var controlClient = null;

    var keyBoard = null;
    var objectTree = null;
    var state = {
        mecanum: {
            go: {
                dir: 0,
                speed: 0
            },
            roll: {
                dir: 0,
                speed: 0
            },
        },
        bed: {
            attitude: {
                auto: false,
                height: 500,
                roll: 0,
                pitch: 0
            },
            velt: {
                go: 0,
                roll: 0
            }
        },
        reset: false
    };
    var display = {
        "使い方": {
            "右上": "ベッド上下",
            "左十": "ベッド姿勢",
            "右十縦": "ベッド前後",
            "右十横": "ベルト",
            "右パッド": "メカナム",
            "右SEL": "カメラ",
            "左SEL": "View"
        },
        "出力": state
    };

    function init() {
        niyarinClient.headBar.updateRobotName("Sample");
        gamePad = niyarinClient.gamePad;
        controlClient = niyarinClient.controlClient;
        keyBoard = new KeyBoardGamepad(KeyBoardGamepad.strToCharCodes("qazwsx"), []);

        var mycamera = new CameraStreamClient("ws://localhost:8080");
        // カメラ mainsub mode 
        niyarinClient.cameraViewer.setMainSubMode([
            mycamera,
            new CameraStreamClient("ws://localhost:8080"),
            new CameraStreamClient("ws://localhost:8080"),
            new CameraStreamClient("ws://localhost:8080")
        ]);
        objectTree = new ObjectTree(document.getElementById("custom-area1"));
        objectTree.init(display);

        window.setInterval(tasks, 50);
    };

    function onMessage(data) {

    }

    function tasks() {
        var buttons = gamePad.getButtons();
        var axes = gamePad.getAxes();
        var pressedButton = gamePad.getPressed();
        var key = gamePad.keyBoardGamepad.getButtons();

        var mecanum = state.mecanum;
        var bed = state.bed;
        bed.velt = {};



        // ベッド前後 
        if (buttons[1]) {
            bed.velt.go = 100;
        } else if (buttons[3]) {
            bed.velt.go = -100;
        }
        // ベルトコンベヤー
        if (buttons[0]) {
            bed.velt.roll = 250;
        } else if (buttons[2]) {
            bed.velt.roll = -250;
        }

        // ベッド上下
        if (buttons[8]) {
            bed.attitude.height += 5;
            if (bed.attitude.height > 1000) bed.attitude.height = 1000;
        } else if (buttons[9]) {
            bed.attitude.height -= 5;
            if (bed.attitude.height < 0) bed.attitude.height = 0;
        }
        // ベッド傾き
        if (buttons[5]) {
            bed.attitude.pitch -= 1;
            if (bed.attitude.pitch < -30) bed.attitude.pitch = -30;
        } else if (buttons[7]) {
            bed.attitude.pitch += 1;
            if (bed.attitude.pitch > 30) bed.attitude.pitch = 30;
        }
        if (buttons[4]) {
            bed.attitude.roll += 1;
            if (bed.attitude.roll > 30) bed.attitude.roll = 30;
        } else if (buttons[6]) {
            bed.attitude.roll -= 1;
            if (bed.attitude.roll < -30) bed.attitude.roll = -30;
        }

        if (pressedButton[10]) {
            mycamera.changeCamera();
            console.log("camera");
        } else if (buttons[11]) {
            bed.attitude.auto = true;
        } else {
            delete bed.attitude.auto;
        }

        // メカナム
        mecanum = {};
        var dir = Math.floor((180.0 * Math.atan2(axes[1], axes[0])) / 3.14159);
        if (buttons[14]) {
            mecanum.roll = {
                dir: 1,
                speed: 250
            };
        } else if (buttons[15]) {
            mecanum.roll = {
                dir: -1,
                speed: 250
            };
        } else {
            mecanum.go = {
                dir: dir + 90,
                speed: Math.ceil(Math.sqrt(axes[0] * axes[0] + axes[1] * axes[1]) * 250)
            };
            if (Math.abs(mecanum.go.speed) < 30) mecanum.go.speed = 0;
        }

        // reset
        if (key[0]) {
            state.reset = true
        } else if (key[1]) {
            state.reset = false
            console.log("not reset");
        } else {
            if ("reset" in state) delete state.reset
        }

        state.mecanum = mecanum;

        if (controlClient.isConnected) {
            controlClient.send(JSON.stringify(state));
        }
        objectTree.update({
            "出力": state
        });
    };

})((this || 0).self || global);