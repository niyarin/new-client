//
//   Gamepad-Wrapper
//   
//   author:niyarin  modified by ukai
//   dependency : 
//      gamepad-mapper.js
//      keyboard-gamepad.js


(function(global) {
    global.GamePad = GamePad;
    global.GamePad.prototype.getPadButtons = getPadButtons;
    global.GamePad.prototype.getPadAxes = getPadAxes;
    global.GamePad.prototype.getAxes = getAxes;
    global.GamePad.prototype.getButtons = getButtons;
    global.GamePad.prototype.getButtonAxes = getButtonAxes;
    global.GamePad.prototype.getPressed = getPressed;

    function GamePad() {
        this.isConnected = false;
        this.gamePadId = null;
        this.mapper = new GamePadMapper();
        this.keyBoardGamepad = new KeyBoardGamepad();
        this.prevPressed = new Array(15);

        window.addEventListener("gamepadconnected", function(e) {
            console.log("gamepad connected", e.gamepad);
            if (!this.isConnected) {
                this.isConnected = true;
                this.gamepadId = e.gamepad.id;
                this.mapper.setGamePad(e.gamepad.id);
                this.prevPressed = new Array(e.gamepad.buttons.length);
            }
        }.bind(this));
        window.addEventListener("gamepaddisconnected", function(e) {
            if (e.gamepad.id == this.gamepadId) {
                console.log("gamepad disconnected");
                this.isConnected = false;
            }
        }.bind(this));
        // 最初から接続されていた場合    
        var a = (navigator.getGamepads ? navigator.getGamepads() :
            (navigator.webkitGetGamepads ? navigator.webkitGetGamepads : []));
        for (var i = 0; i < a.length; i++) {
            var v = a[i];
            if (v == undefined) return false;
            else {
                console.log("gamepad is " + v.id);
                this.isConnected = true;
                this.gamepadId = v.id;
                this.mapper.setGamePad(v.id);
                this.prevPressed = new Array(v.buttons.length);
                return true;
            }
        }
    };

    function getPadButtons() {
        var gamepads = navigator.getGamepads() || [];
        for (var i = 0; i < gamepads.length; i++) {
            var gp = gamepads[i];
            if (gp.id == this.gamepadId) {
                var buttons = gp.buttons.map(function(x) {
                    return x.pressed;
                });
                return this.mapper.mapButtons(buttons);
            }
        }
        return null;
    };

    function getPadAxes() {
        var gamepads = navigator.getGamepads() || [];
        for (var i = 0; i < gamepads.length; i++) {
            var gp = gamepads[i];
            if (gp.id == this.gamepadId) {
                return this.mapper.mapAxes(gp.axes);
            }
        }
        return null;
    };


    function getAxes() {
        if (this.isConnected) {
            return this.getPadAxes();
        } else {
            return this.keyBoardGamepad.getAxes();
        }
    };

    function getButtons() {
        if (this.isConnected) {
            return this.getPadButtons();
        } else {
            return this.keyBoardGamepad.getButtons();
        }
    };

    function getPressed() {
        var gb = this.getButtons();
        var ret = [];
        for (var i = 0; i < gb.length; i++) {
            ret.push(gb[i] ^ this.prevPressed[i] && gb[i]);
            this.prevPressed[i] = gb[i];
        }
        return ret;
    }

    function getButtonAxes() {
        return [this.getButtons, this.getAxes];
    };

})((this || 0).self || global);