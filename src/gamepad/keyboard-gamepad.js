//
// ゲームパッドのようにキーボードを使うためのクラス
//
// dependency :
//  none
// usage : 
//  new KeyBoardGamepad(buttonsPattern?, axesPattern?) で作る
//  引数によって、使用するキーマッピングを設定できる
//  引数をなしにすると下のdefaultButtonsPattern, defaultAxesPatternが使われる
//  引数のフォーマットは下のdefault...参照
//  KeyBoardGamepad.getButtons(), KeyBoardGamepad.getAxes(), KeyBoardGamepad.getButtonsPressed() で状態が得られる

(function(global) {
    // public
    global.KeyBoardGamepad = KeyBoardGamepad; // ([Number]?, [[Number, Number]]?) => KeyBoardGamepad    
    global.KeyBoardGamepad.prototype.getButtons = getButtons; // () => [bool]
    global.KeyBoardGamepad.prototype.getButtonsPressed = getButtonsPressed; // () => [bool]
    global.KeyBoardGamepad.prototype.getAxes = getAxes; // () => [Number]
    global.KeyBoardGamepad.strToCharCodes = function(str) {
        var res = [];
        for (var i = 0; i < str.length; i++) {
            res.push(str.charCodeAt(i));
        }
        return res;
    }

    // private 

    // キーコードとのマッピング
    var defaultButtonsPattern = [48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 81, 87, 69, 82, 84, 89]; // 0 ~ 9 q w e r t y
    var defaultAxesPattern = [
        [39, 37], // <- ->
        [40, 38], // v ^
        [76, 72], // l h (vim)
        [74, 75] // j k
    ]; // [軸正, 軸負] の順番

    function KeyBoardGamepad(buttonsPattern, axesPattern) {
        this.buttonsPattern = buttonsPattern == null ? defaultButtonsPattern : buttonsPattern;
        this.axesPattern = axesPattern == null ? defaultAxesPattern : axesPattern;
        this.state = {
            buttons: new Array(this.buttonsPattern.length),
            buttonsPressed: new Array(this.buttonsPattern.length),
            axes: new Array(this.axesPattern.length),
        };
        for (var i = 0; i < this.state.buttons.length; i++) this.state.buttons[i] = false;
        for (var i = 0; i < this.state.buttonsPressed.length; i++) this.state.buttonsPressed[i] = false;
        for (var i = 0; i < this.state.axes.length; i++) this.state.axes[i] = 0;

        window.addEventListener("keydown", onKeyDown.bind(this));
        window.addEventListener("keyup", onKeyUp.bind(this));
    }

    function getButtons() {
        return [].concat(this.state.buttons);
    }

    function getButtonsPressed() {
        var ret = [].concat(this.state.buttonsPressed);
        this.state.buttonsPressed.forEach(function(v, i, a) {
            a[i] = false;
        });
        return ret;
    }

    function getAxes() {
        return [].concat(this.state.axes);
    }

    function onKeyDown(e) {
        this.buttonsPattern.forEach(function(keyCode, i) {
            if (e.keyCode == keyCode) {
                this.state.buttons[i] = true;
                this.state.buttonsPressed[i] = true;
            }
        }.bind(this));
        this.axesPattern.forEach(function(keyCodes, i) {
            if (e.keyCode == keyCodes[0]) {
                this.state.axes[i] = 1;
            } else if (e.keyCode == keyCodes[1]) {
                this.state.axes[i] = -1;
            }
        }.bind(this));
    }

    function onKeyUp(e) {
        this.buttonsPattern.forEach(function(keyCode, i) {
            if (e.keyCode == keyCode) {
                this.state.buttons[i] = false;
                this.state.buttonsPressed[i] = false;
            }
        }.bind(this));
        this.axesPattern.forEach(function(keyCodes, i) {
            if (e.keyCode == keyCodes[0] || e.keyCode == keyCodes[1]) {
                this.state.axes[i] = 0;
            }
        }.bind(this));
    }
})((this || 0).self || global);