/*
    dependency :
        none
    usage :
        new GamePadMapper() する
        setGamePad(gamepadId); でマッピングを設定(インスタンス生成時にnew GamePadMapper(gamepadId)でもOK)
        mapButtons(buttons), mapAxes(axes) で引数の配列をマッピングに応じて並び替えた(新しい)配列を返す
        ゲームパッドのマッピング定義が存在しない場合、デフォルト設定（引数の並びをそのまま返す）になる
        マッピング定義があるか、デフォルトが使われているかは doesRecognized()で得られる
    note : 
        以下の状態になるようにマッピングしている
        右十字キー 
        右 : 0
        上 : 1
        左 : 2
        下 : 3
        左十字キー
        右 : 4
        上 : 5
        左 : 6
        下 : 7
        右上キー
        上 : 8
        下 : 9
        右下キー
        上 : 10
        下 : 11
        アナログパッド押下
        上 : 12
        下 : 13
        真ん中あたりのキー
        右 : 14
        左 : 15
 */

(function(global) {
    // public
    global.GamePadMapper = GamePadMapper; // (gamepadId?) => GamePadMapper    
    GamePadMapper.prototype.setGamePad = setGamePad; // (gamapadId) => void    
    GamePadMapper.prototype.mapButtons = mapButtons; // ([bool]) => [bool]     
    GamePadMapper.prototype.mapAxes = mapAxes; // ([number]) => [number]    
    GamePadMapper.prototype.doesRecognized = doesRecognized; // () => bool

    // private    

    // APIでi番として認識されるボタンをj番として使いたい場合 buttons[j] = i とする
    var mappingList = {
        default: {
            buttons: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],
            axes: [0, 1, 2, 3]
        },
        windows: {
            firefox: {
                "046d-c216-Logicool Dual Action": {
                    buttons: [2, 3, 0, 1, 16, 13, 15, 14, 5, 7, 4, 6, 11, 10, 8, 9],
                    axes: [2, 3, 0, 1]
                },
                "xinput": {
                    buttons: [1, 3, 2, 0, 15, 12, 14, 13, 5, 7, 4, 6, 11, 10, 9, 8],
                    axes: [2, 3, 0, 1]
                },
                "0925-8888-JC-PS101U": {
                    buttons: [1, 0, 3, 2, 15, 12, 14, 13, 7, 5, 6, 4, 11, 10, 8, 9],
                    axes: [2, 3, 0, 1]
                }
            }
        },
        macintosh: {
            firefox: {
                "46d-c216-Logicool Dual Action": {
                    buttons: [2, 3, 0, 1, 15, 12, 14, 13, 5, 7, 4, 6, 11, 10, 9, 8],
                    axes: [3, 4, 1, 2]
                },
                "925-8888-JC-PS101U": {
                    buttons: [1, 0, 3, 2, 15, 12, 14, 13, 7, 5, 6, 4, 11, 10, 8, 9],
                    axes: [2, 3, 0, 1]
                },
                "5b8-1006-Elecom Wireless Gamepad": {
                    buttons: [3, 1, 0, 2, 16, 13, 15, 14, 5, 7, 4, 6, 11, 10, 9, 8],
                    axes: [2, 3, 0, 1]
                }
            },
            chrome: {
                "Logicool Dual Action (STANDARD GAMEPAD Vendor: 046d Product: c216)": {
                    buttons: [1, 3, 2, 0, 15, 12, 14, 13, 5, 7, 4, 6, 11, 10, 9, 8],
                    axes: [3, 4, 1, 2]
                },
                "JC-PS101U (Vendor: 0925 Product: 8888)": {
                    buttons: [1, 0, 3, 2, 15, 12, 14, 13, 7, 5, 6, 4, 11, 10, 8, 9],
                    axes: [2, 5, 0, 1]
                }
            },
            safari: {

            }
        },
        linux: {
            firefox: {
                "0925-8888-WiseGroup.,Ltd JC-PS101U": {
                    buttons: [1, 0, 3, 2, 15, 12, 14, 13, 7, 5, 6, 4, 11, 10, 8, 9],
                    axes: [2, 3, 0, 1]
                }
            }
        }
    };

    // 補助関数
    function getOS() {
        var ua = global.navigator.userAgent.toLowerCase();
        var os = ["windows", "macintosh", "linux"];
        for (var i = 0; i < os.length; i++) {
            if (ua.indexOf(os[i]) != -1) return os[i];
        }
        return null;
    }

    function getBrowser() {
        var ua = global.navigator.userAgent.toLowerCase();
        var b = ["firefox", "chrome", "safari"];
        for (var i = 0; i < b.length; i++) {
            if (ua.indexOf(b[i]) != -1) return b[i];
        }
        return null;
    }

    // 公開メゾッド実装

    function GamePadMapper(gamepadId) {
        this.setGamePad(gamepadId);
    }

    function setGamePad(gamepadId) {
        var os = getOS();
        var browser = getBrowser();
        if (gamepadId && os in mappingList && browser in mappingList[os] && gamepadId in mappingList[os][browser]) {
            this.mapping = mappingList[os][browser][gamepadId];
        } else {
            this.mapping = mappingList.default;
            console.log("Gamepad " + gamepadId + " mapping is not assigned. default setting is enabled.");
        }
    }

    function mapButtons(buttons) {
        var ret = new Array(16);
        for (var i = 0; i < ret.length; i++) {
            ret[i] = buttons[this.mapping.buttons[i]];
        }
        return ret;
    }

    function mapAxes(axes) {
        var ret = new Array(4);
        for (var i = 0; i < ret.length; i++) {
            ret[i] = axes[this.mapping.axes[i]];
        }
        return ret;
    }

    function doesRecognized() {
        return this.mapping === mappingList.default;
    }
})((this || 0).self || global);