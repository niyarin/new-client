var CommunicationDrawingBoard = function(id, ops) {
    if (ops) ops.strorage = false // これでいい？


    this.super = DrawingBoard.Board.prototype;

    this.image_canvas = {};
    this.isConnected = false;
    this.isInited = false;
    if (ops.url) {
        this.ws = new WebSocket('ws://' + ops.url, 'drawing');
    } else {
        this.ws = new WebSocket('ws://' + location.host, 'drawing');
    }

    DrawingBoard.Board.call(this, id, ops);

    var ws_on_receive = {};
    ws_on_receive['connection_init'] = this.onConnectInit;
    ws_on_receive['canvas'] = this.onCanvasInitByServer;
    ws_on_receive['draw'] = this.onServerDraw;
    ws_on_receive['update_image'] = this.onImgReceive;
    var that = this;
    this.ws.onopen = $.proxy(this.onConnect, this);
    this.ws.onclose = $.proxy(this.onDisconnect, this);
    this.ws.onerror = $.proxy(this.onConnectError, this);
    this.ws.onmessage = function(ev) {
        var data = JSON.parse(ev.data);
        //console.log("receive", data);
        if (data.type in ws_on_receive) ws_on_receive[data.type].call(that, data.data);
    }
};

Object.setPrototypeOf(CommunicationDrawingBoard.prototype,
    DrawingBoard.Board.prototype);


// - - - - - - - - - - - - - - - - - - - -
// - - - - - 関数オーバーライド  - - - - -
// - - - - - - - - - - - - - - - - - - - -

// @override
CommunicationDrawingBoard.prototype.draw = function() {

    if (this.isDrawing) {
        var w = this.dom.$canvas.width();
        var h = this.dom.$canvas.height();
        var midP = this._getMidInputCoords(this.coords.current);
        this.sendDrawDataToServer({
            type: 'curve',
            color: this.ctx.strokeStyle,
            alpha: this.ctx.globalAlpha,
            width: this.ctx.lineWidth / w,
            comp: this.ctx.globalCompositeOperation,
            coords: [{
                x: this.coords.old.x / w,
                y: this.coords.old.y / h
            }, {
                x: this.coords.oldMid.x / w,
                y: this.coords.oldMid.y / h
            }, {
                x: midP.x / w,
                y: midP.y / h
            }]
        });
    }
    this.super.draw.call(this);
};

// @override
CommunicationDrawingBoard.prototype.fill = function(e) {
    var w = this.dom.$canvas.width();
    var h = this.dom.$canvas.height();
    this.sendDrawDataToServer({
        type: 'fill',
        coords: {
            x: this.coords.current.x / w,
            y: this.coords.current.y / h
        },
        color: this.ctx.strokeStyle
    });
    this.super.fill.call(this, e);
};

// @override
CommunicationDrawingBoard.prototype.reset = function(opts) {
    this.sendDrawDataToServer({
        type: 'reset'
    });
    this.super.reset.call(this, opts);
};

// @override

CommunicationDrawingBoard.prototype.resize = function() {
    this.super.resize.call(this);
};

// @override
CommunicationDrawingBoard.prototype.goBackInHistory = function() {
    this.sendDrawDataToServer({
        type: 'go_back'
    });
    this.super.goBackInHistory.call(this);
};

// @override
CommunicationDrawingBoard.prototype.goForthInHistory = function() {
    this.sendDrawDataToServer({
        type: 'go_forth'
    });
    this.super.goForthInHistory.call(this);
};

// @override
CommunicationDrawingBoard.prototype.saveHistory = function() {
    this.sendDrawDataToServer({
        type: 'save_history'
    });
    this.super.saveHistory.call(this);
};

//@override
CommunicationDrawingBoard.prototype.setBackgroundImg = function(src, opts) {
    this.sendDrawDataToServer({
        type: 'set_background',
        background: src
    });
    opts = $.extend({
        stretch: true
    })
    this.image_canvas.background = src;
    this.super.setBackgroundImg.call(this, src, opts);
};



// 基本関数

/*
   protcol : websocket protcol
   data    : data (object)
   (自動で client_idを付加するため)
 */
CommunicationDrawingBoard.prototype.sendDataToServer = function(protcol, data) {
    if (this.isConnected && typeof this.client_id !== "undefined") {
        data = (typeof data !== "undefined" ? data : {});
        data.client_id = this.client_id;
        this.ws.send(JSON.stringify({
            type: protcol,
            data: data
        }));
    }
};

CommunicationDrawingBoard.prototype.sendDrawDataToServer = function(data) {
    if (this.isInited) {
        data.canvas_id = this.image_canvas.id;
        this.sendDataToServer('draw', data);
    }
};

CommunicationDrawingBoard.prototype.initCanvas = function(canvas) {
    console.log('canvas init');
    this.image_canvas = canvas;
    this.super.reset.call(this, {
        webStorage: false,
        history: false,
        background: true
    });
    this.setImg(canvas.canvas);
    this.resetBackground(canvas.background);
    this.initHistory();
    console.log(this.opts);
    this.isInited = true;
};

CommunicationDrawingBoard.prototype.changeCanvas = function(canvas_id) {
    this.isInited = false;
    this.sendDataToServer('get_canvas', {
        id: canvas_id
    });
};

CommunicationDrawingBoard.prototype.saveCanvasToServer = function() {
    if (!this.isInited) return;
    this.sendDataToServer('save', {
        canvas: this.image_canvas,
        img: this.getForgroundImg()
    });
};


// - - - - - - - - - - - - - - - - - - - -
// - - - - - コールバック関数（接続関係）-
// - - - - - - - - - - - - - - - - - - - -

CommunicationDrawingBoard.prototype.onConnect = function() {
    console.log("connected to server (" + this.url + ")");
};

CommunicationDrawingBoard.prototype.onConnectError = function(e) {
    console.log("connect error to server (" + this.url + ") Error : " + e);
};

CommunicationDrawingBoard.prototype.onConnectInit = function(data) {
    console.log("connect inited to server (" + this.url + ")");
    this.isConnected = true;
    this.client_id = data.client_id;
    //this.sendDataToServer('get_canvas');
    this.isInited = true
};

CommunicationDrawingBoard.prototype.onDisconnect = function() {
    console.log("disconnected from server (" + this.url + ")");
    this.isInited = false;
    this.super.reset.call(this, {
        webStorage: false
    });
    this.isConnected = false;
};


// - - - - - - - - - - - - - - - - - - - -
// - - - - - コールバック関数（動作系）  -
// - - - - - - - - - - - - - - - - - - - -

// サーバーからの情報に従って描写
CommunicationDrawingBoard.prototype.onServerDraw = function(data) {
    var self = this;
    //if (data.client_id == self.client_id) return;
    //if (data.canvas_id != self.image_canvas.id) return;
    var func = {
        curve: function() {
            var w = self.dom.$canvas.width();
            var h = self.dom.$canvas.height();

            var oldStrokeStyle = self.ctx.strokeStyle;
            var oldLineWidth = self.ctx.lineWidth;
            self.ctx.strokeStyle = data.color;
            self.ctx.lineWidth = Math.ceil(data.width * w);
            self.ctx.globalCompositeOperation = data.comp;

            self.ctx.beginPath();
            self.ctx.moveTo(data.coords[2].x * w, data.coords[2].y * h);
            self.ctx.quadraticCurveTo(
                data.coords[0].x * w, data.coords[0].y * h,
                data.coords[1].x * w, data.coords[1].y * h);
            self.ctx.stroke();

            self.ctx.strokeStyle = oldStrokeStyle;
            self.ctx.lineWidth = oldLineWidth;
        },
        fill: function() {
            var w = self.dom.$canvas.width();
            var h = self.dom.$canvas.height();

            var p = {};
            p.coords = {
                x: data.coords.x * w,
                y: data.coords.y * h
            };
            var oldStrokeStyle = self.ctx.strokeStyle;
            self.ctx.strokeStyle = data.color;
            self.super.fill.call(self, p);
            self.ctx.strokeStyle = oldStrokeStyle;
        },
        reset: function() {
            self.super.reset.call(self);
        },
        go_back: function() {
            self.super.goBackInHistory.call(self);
        },
        go_forth: function() {
            self.super.goForthInHistory.call(self);
        },
        save_history: function() {
            self.super.saveHistory.call(self);
        },
        set_background: function() {
            this.image_canvas.background = data.background;
            self.super.setBackgroundImg.call(self, data.background, {
                stretch: true
            });
        }
    };
    if (data.type in func) func[data.type]();
    else console.log("onServerDraw : not implemented type :" + data.type);
};

/*
CommunicationDrawingBoard.prototype.onImgReceive= function(data) {
    // 現在の画像の更新としての、画像データが届いた場合    
    console.log("received new image");
    this.setImg(data.img);
};
*/

CommunicationDrawingBoard.prototype.onCanvasChange = function(data) {
    // キャンバス変更通知が来た時
    // TODO : ここはオプションにすべき
    console.log("received canvas change info");
    // 変更に従う場合
    this.isInited = false;
    // 変更情報を要求
    this.sendDataToServer('get_canvas', {
        id: data.id
    });
};

CommunicationDrawingBoard.prototype.onCanvasInitByServer = function(canvas) {
    console.log("canvas init by server");

    if (this.isInited == true) return;
    // キャンバス変更情報が来た時
    this.initCanvas(canvas);
};