/*
    usage : 
    dependency:
        jquery
        style.css
 */


(function(global) {
    // public
    global.HeadBar = HeadBar;

    function HeadBar(elem, controlClient) {
        this.initView(elem);
        if (controlClient) {
            controlClient.onMessage(controlClientOnMessage.bind(this));
            controlClient.onConnectionChange(controlClientOnChange.bind(this));
        }
        this.state = {
            connectionServer: false,
            connectionTpip: false,
            robotName: "",
            serverAddr: "",
            tpipAddr: "",
            wifiPower: 0,
            logicBattery: 0,
            driveBattery: 0
        };
    }
    HeadBar.prototype.initView = initView;
    HeadBar.prototype.updateConnectionServer = updateConnectionServer;
    HeadBar.prototype.updateConnectionTpip = updateConnectionTpip;
    HeadBar.prototype.updateRobotName = updateRobotName;
    HeadBar.prototype.updateServerAdder = updateServerAdder;
    HeadBar.prototype.updateTpipAdder = updateTpipAdder;
    HeadBar.prototype.updateWifiPower = updateWifiPower;
    HeadBar.prototype.updateLogicBattery = updateLogicBattery;
    HeadBar.prototype.updateDriveBattery = updateDriveBattery;
    // private
    HeadBar.prototype._updateConnection = _updateConnection;

    function initView(elem) {
        this.container = $(elem);
        this.container.addClass("flex-row-group head-bar");
        this.prev = {};
        this.robot = {
            name: $('<strong>?</strong>').html("none"),
            tpipAddr: $('<strong>?</strong>'),
            serverAddr: $('<strong>?</strong>')
        }
        this.connection = {
            server: $('<i class="fa fa-close"></i>'),
            tpip: $('<i class="fa fa-close"></i>')
        }
        this.battery = {
            logic: {
                icon: $('<i class="fa fa-battery-full"></i>'),
                val: $('<div></div>').addClass('battery')
            },
            drive: {
                icon: $('<i class="fa fa-battery-full"></i>'),
                val: $('<div></div>').addClass('battery')
            }
        }
        this.wifiPower = $('<span>0</span>');

        var pairDiv = '<div class="head-bar-pair-div"></div>';

        var statContainer1 = $('<div class="flex-row-list"></div>');
        statContainer1.append([
            $(pairDiv)
            .append("<small>Server:</small>")
            .append(this.connection.server),
            $(pairDiv)
            .append("<small>Tpip:</small>")
            .append(this.connection.tpip)
        ]);
        var robotNameContainer = $('<div class="flex-row-list"></div>');
        robotNameContainer.append([
            $(pairDiv)
            .append("<small>Robot:</small>")
            .append(this.robot.name),
            $(pairDiv)
            .append("<small>Server:</small>")
            .append(this.robot.serverAddr),
            $(pairDiv)
            .append("<small>Tpip:</small>")
            .append(this.robot.tpipAddr),
        ]);
        var statContainer2 = $('<div class="flex-row-list"></div>');
        statContainer2.css("justify-content", "flex-end");
        statContainer2.append([
            $(pairDiv)
            .append('<i class="fa fa-wifi"></i>')
            .append(this.wifiPower),
            $(pairDiv)
            .append("Logic ")
            .append(this.battery.logic.val),
            $(pairDiv)
            .append("Drive ")            
            .append(this.battery.drive.val),
        ]);
        this.container.append([
            statContainer1, robotNameContainer, statContainer2
        ]);
    }

    function _updateConnection(target, flg) {
        if (flg) {
            target
                .removeClass("fa-close")
                .addClass("fa-circle-o");
        } else {
            target
                .removeClass("fa-circle-o")
                .addClass("fa-close");
        }
    }

    function updateConnectionServer(flg) {
        this._updateConnection(this.connection.server, flg);
    }

    function updateConnectionTpip(flg) {
        this._updateConnection(this.connection.tpip, flg);
    }

    function updateRobotName(name) {
        this.robot.name.html(name);
    }

    function updateServerAdder(ip) {
        this.robot.serverAddr.html(ip);
    }

    function updateTpipAdder(ip) {
        this.robot.tpipAddr.html(ip);
    }

    function updateWifiPower(power) {
        this.wifiPower.html(power);
    }

    function updateLogicBattery(val) {
        var str = val.toFixed(1);
        this.battery.logic.val.html(str + " V");
    }

    function updateDriveBattery(val) {
        this.battery.drive.val.html(val.toFixed(2) + " V");
    }

    function controlClientOnMessage(data) {
        var that = this;
        var update = function(key, val, f) {
            if (that.state[key] != val) {
                f.call(that, val);
                that.state[key] = val;
            }
        };
        if ("connection" in data) {
            update("connectionTpip", data.connection.tpip, this.updateConnectionTpip);
        }
        if ("battery" in data) {
            update("logicBattery", data.battery, this.updateLogicBattery);
        }
        if ("target" in data) {
            update("tpipAddr", data.target, this.updateTpipAdder);
        }
    }

    function controlClientOnChange(flg) {
        if (this.state["connectionServer"] != flg) {
            this.updateConnectionServer(flg);
            if(!flg) this.updateConnectionTpip(false);
            this.state["connectionServer"] = flg;
        }
    }

})((this || 0).self || global);