/*
    usage : 
        new PadState(elem) : elem is empty div
        PadState#setButtons(buttons) or PadState#updateButtons(buttons) :update buttons status
        PadState#setAxes(axes) or PadState#updateAxes(axes) :update axes status
    dependency:
        jquery
        style.css
        pad-state.css
 */

(function(global) {
    // public
    global.PadState = PadState; // (elem, buttonNum?, axesNum?) => PadState
    global.PadState.prototype.updateButtons = updateButtons;
    global.PadState.prototype.updateAxes = updateAxes;

    //alias
    global.PadState.prototype.setButtons = updateButtons;
    global.PadState.prototype.setAxes = updateAxes;
    global.PadState.prototype.update = function() {};

    // private 
    function PadState(elem, buttonNum, axesNum) {
        if (buttonNum == null) buttonNum = 16;
        if (axesNum == null) axesNum = 2;
        this.container = $(elem);
        this.container.addClass("flex-row-list");
        this.container.append("<div>GamePad</div>");
        var axesCircle = new Array(axesNum);
        this.axes = new Array(axesNum);
        for (var i = 0; i < axesNum; i++) {
            axesCircle[i] = $('<div class="pad-state-axes"></div>')
            this.axes[i] = $('<div class="pad-state-axes-dot"/>').get(0);
            axesCircle[i].append(this.axes[i]);
        }
        this.container.append(axesCircle);

        this.buttons = new Array(buttonNum);
        this.prevButtons = new Array(buttonNum);
        for (var i = 0; i < buttonNum; i++) {
            this.buttons[i] = $('<div class="pad-state-circle pad-state-default">' + i + '</div>');
        }
        this.container.append(this.buttons);
    }

    function updateButtons(buttons) {
        for (var i = 0; i < Math.min(this.buttons.length, buttons.length); i++) {
            if (buttons[i] != this.prevButtons[i]) {
                this.prevButtons[i] = buttons[i];
                if (buttons[i]) {
                    this.buttons[i].removeClass("pad-state-default");
                    this.buttons[i].addClass("pad-state-pressed");
                } else {
                    this.buttons[i].removeClass("pad-state-pressed");
                    this.buttons[i].addClass("pad-state-default");
                }
            }
        }
    }

    function updateAxes(axes) {
        var w = this.axes[0].width;
        var h = this.axes[0].height;
        for (var i = 0; i < Math.min(this.axes.length, axes.length / 2); i++) {
            var x = axes[i * 2] * 90;
            var y = axes[i * 2 + 1] * 90;
            var r = Math.sqrt(x * x + y * y);
            if (r > 90) {
                x = x / r * 90;
                y = y / r * 90;
            }
            this.axes[i].style.top = y + "%";
            this.axes[i].style.left = x + "%";
        }
    }

})((this || 0).self || global);