/*
    usage :                         
    dependency:
        jquery
        style.css
 */

(function(global) {
    global.ObjectTree = ObjectTree;
    global.ObjectTree.prototype.init = init;
    global.ObjectTree.prototype.update = update;

    function ObjectTree(elem) {
        this.elem = elem;
    }
    // data のキー, 値に対して再帰的に探索してTreeViewを作る
    function init(data) {
        this.elem.innerHTML = null;
        var dfs = function(key, val) {
            if (typeof val !== "object") {
                var ks = document.createElement('span');
                ks.innerText = key;
                var vs = document.createElement('span');
                vs.innerText = val;
                var li = document.createElement('li');
                li.classList.add("head-bar-pair-div");
                li.style.listStyleType = "none";
                li.style.paddingLeft = "10px";
                li.appendChild(ks);
                li.appendChild(vs);
                return [vs, li];
            } else {
                var ks = document.createElement('span');
                ks.innerText = key;
                var vs = {};
                var ul = document.createElement('ul');
                ul.style.listStyleType = "circle";
                ul.style.paddingLeft = "10px";
                Object.keys(val).forEach(function(key, i) {
                    var ret = dfs(key, val[key]);
                    vs[key] = ret[0];
                    ul.appendChild(ret[1]);
                });
                var li = document.createElement('li');
                li.appendChild(ks);
                li.appendChild(ul);
                return [vs, li];
            }
        };

        this.dataSpan = {};
        var ul = document.createElement('ul');
        ul.style.listStyleType = "circle";
        ul.style.paddingLeft = "10px";

        Object.keys(data).forEach(function(key, i) {
            var ret = dfs(key, data[key]);
            this.dataSpan[key] = ret[0];
            ul.appendChild(ret[1]);
        }.bind(this));
        this.elem.appendChild(ul);
    }

    // data に入っている分だけを更新
    function update(data) {
        var dfs = function(data, obj) {
            if (typeof data !== "object") obj.innerText = data;
            else {
                var keys = Object.keys(data);
                for (var i = 0; i < keys.length; i++) {
                    if (keys[i] in obj) dfs(data[keys[i]], obj[keys[i]]);
                }
            }
        }
        dfs(data, this.dataSpan);
    }
})((this || 0).self || global);