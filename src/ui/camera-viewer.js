/*
    usage : 
        new CameraViewer(element) で element内部にDOM生成
        setSingleMode(cameraStreamClient), setSplitMode, setMainSubMode で画像表示
    dependency:
        jquery
        style.css
        cameraStreamClient
 */

(function (global) {
    // public
    global.CameraViewer = CameraViewer; // (elem) => object
    var cv = global.CameraViewer;
    cv.prototype.setSingleMode = setSingleMode;
    cv.prototype.setSplitMode = setSplitMode;
    cv.prototype.setMainSubMode = setMainSubMode;

    var defaultLoadingImagePath = "../../img/ball-triangle.svg";
    var subLoadingImagePath = "../../img/rings.svg";


    // public
    function CameraViewer(elem) {
        this.elem = $(elem);
        this.elem.addClass("flex-colmn-group");
        this.elem.height("100%").width("100%");
    }

    function setSingleMode(cameraStreamClient) {
        var img = $('<img class="camera-viewer-image"></img>');
        var imgDOM = img.get(0);
        if (typeof cs == "string") {
            // mjpeg image
            imageSetCameraImageMode(img);
            imgDOM.src = cameraStreamClient;
        } else {
            imageSetLoadingImageMode(img);

            cameraStreamClient.onImageReceived(function (objUrl) {
                imgDOM.src = objUrl;
            });

            cameraStreamClient.onConnectionChanged(function (isConnected) {
                if (!isConnected) imageSetLoadingImageMode(img);
                else imageSetCameraImageMode(img);
            });
        }

        var row = $('<div class="flex-row-group camera-viewer-flex-row-group"></div>')
            .css("max-width", "100%").css("max-height", "100%")
        this.elem.empty();
        this.elem.append(row.append(img));
    }

    function setSplitMode(cameraStreamClients) {
        var row1 = $('<div class="flex-row-group camera-viewer-flex-row-group"></div>').height("49%").width("99%"),
            row2 = $('<div class="flex-row-group camera-viewer-flex-row-group"></div>').height("49%").width("99%");
        var addCamera = function (row, begin, end) {
            for (var i = begin; i < end; i++) {
                if (cameraStreamClients.length > i) {
                    (function (cs) {
                        var img = $('<img class="camera-viewer-image"></img>');
                        var imgDOM = img.get(0);
                        if (typeof cs === "string") {
                            // mjpeg image
                            imageSetCameraImageMode(img);
                            imgDOM.src = cs;
                        } else {
                            imageSetLoadingImageMode(img);

                            cs.onImageReceived(function (objUrl) {
                                imgDOM.src = objUrl;
                            });

                            cs.onConnectionChanged(function (isConnected) {
                                if (!isConnected) imageSetLoadingImageMode(img);
                                else imageSetCameraImageMode(img);
                            });
                        }
                        img.appendTo(row);
                    })(cameraStreamClients[i]);
                }
            }
        };
        addCamera(row1, 0, 2);
        addCamera(row2, 2, 4);
        this.elem.empty();
        this.elem.append([row1, row2]);
    }

    function setMainSubMode(cameraStreamClients) {
        var row1 = $('<div class="flex-row-group camera-viewer-flex-row-group"></div>').height("69%").width("99%"),
            row2 = $('<div class="flex-row-group camera-viewer-flex-row-group"></div>').height("29%").width("99%");
        var addCamera = function (row, begin, end) {
            for (var i = begin; i < end; i++) {
                if (cameraStreamClients.length > i) {
                    (function (cs) {
                        var img = $('<img class="camera-viewer-image"></img>');
                        var imgDOM = img.get(0);
                        img.css("max-width", end - begin == 1 ? "100%" : "30%");
                        if (typeof cs === "string") {
                            // mjpeg image
                            imageSetCameraImageMode(img);
                            imgDOM.src = cs;
                        } else {
                            imageSetLoadingImageMode(img, i == 0 ? null : subLoadingImagePath);

                            cs.onImageReceived(function (objUrl) {
                                imgDOM.src = objUrl;
                            });

                            var ii = i;
                            cs.onConnectionChanged(function (isConnected) {
                                if (!isConnected) {
                                    imageSetLoadingImageMode(img, ii == 0 ? null : subLoadingImagePath);
                                } else {
                                    imageSetCameraImageMode(img);
                                    if (ii > 0) {
                                        cs.send(JSON.stringify({
                                            "fps": 5
                                        }));
                                    }
                                }
                            });
                        }
                        row.append(img);
                    })(cameraStreamClients[i]);
                }
            }
        };
        addCamera(row1, 0, 1);
        addCamera(row2, 1, 4);
        this.elem.empty();
        this.elem.append([row1, row2]);
    }

    // private

    function getFilePath() {
        var scripts = document.getElementsByTagName("script");
        for (var i = 0; i < scripts.length; i++) {
            var match = scripts[i].src.match(/(^|.*\/)camera-viewer\.js$/);
            if (match) return match[1];
        }
        return null;
    }

    function imageSetLoadingImageMode(image, loadingImage) {
        if (loadingImage == null) loadingImage = defaultLoadingImagePath;
        image.attr("src", null);
        image.attr("src", getFilePath() + loadingImage);
        image.addClass("camera-viewer-image-loading");
        image.removeClass("camera-viewer-image-camera");
    }

    function imageSetCameraImageMode(image) {
        //image.attr("src", null);
        image.removeClass("camera-viewer-image-loading");
        image.addClass("camera-viewer-image-camera");
    }


})((this || 0).self || global);