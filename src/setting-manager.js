(function(global) {
    // public
    global.SettingManager = SettingManager;
    var sm = SettingManager;
    var defaultUrl = location.protocol == "file:" ? "localhost:8080" : location.host;

    var defaultSettings = {        
        control: defaultUrl,
        camera: defaultUrl,
        sound: defaultUrl
    };

    function SettingManager(managerUrl) {
        this.setting = defaultSettings;
        this._fetchSetting(managerUrl);
    }

    sm.prototype.getSetting = function(key) {
        return this.setting[key];
    };

    sm.prototype._fetchSetting = function(url) {
        $.get(url, {}, function(res) {
            this.setting = res;
            console.log(res);
        });
    };

    sm.prototype.setSetting = function(setting) {
        $.extend(this.setting, setting);
    }

})((this || 0).self || global);