//
// dependent files:
//     tools.js
//     gamepad.js
//
// author:niyarin
//

niyarinClient = {
    controlClient: null,
    gamePad: null,
    settingManager: null,
    // ui
    headBar: null,
    padState: null,
    cameraViewer: null,
    objectTree: null,
};

$(document).ready(function () {
    function getFilePath() {
        var scripts = document.getElementsByTagName("script");
        for (var i = 0; i < scripts.length; i++) {
            var match = scripts[i].src.match(/(^|.*\/)main\.js$/);
            if (match) return match[1];
        }
        return null;
    }

    var settingManager = new SettingManager("http://192.168.22.1:5000");
    niyarinClient.settingManager = settingManager;
    niyarinClient.cameraViewer = new CameraViewer(document.getElementById("camera-viewer"));
    niyarinClient.controlClient = new ControlClient("ws://" + settingManager.getSetting("control"));
    niyarinClient.gamePad = new GamePad();
    niyarinClient.padState = new PadState(document.getElementById("pad-state"));
    niyarinClient.headBar = new HeadBar(document.getElementById("head1"), niyarinClient.controlClient);

    var imageBoard = new CommunicationDrawingBoard('share-board', {
        stretchImg: true,
        controls: false,
        stretchBackgroundImg: true,
        background: getFilePath() + "../img/field.png",
        url: "192.168.22.11:3000"
    });

    YourFile.init();
    niyarinClient.headBar.updateServerAdder(settingManager.getSetting("control"));
    niyarinClient.controlClient.onMessage(YourFile.onMessage);

    niyarinClient.timeClient = new TimeClient("192.168.22.11:3000");
    niyarinClient.timeClient.on_start = function () {
        document.getElementById("message").innerHTML = "競技開始！";
    };
    niyarinClient.timeClient.on_end = function () {
        document.getElementById("message").innerHTML = "競技終了";
    };
    niyarinClient.timeClient.on_update = function (left) {
        if (left >= 0) {
            var m = Math.floor(left / 60);
            var s = left % 60;
            document.getElementById("message").innerHTML = "残り時間（推定）　：　" + ("0" + m).slice(-2) + ":" + ("0" + s).slice(-2);
        } else {
            document.getElementById("message").innerHTML = "超過時間　：　" + ( -1 * left) + "s";
        }
    };


    (function loop() {
        niyarinClient.padState.connect = niyarinClient.gamePad.isConnected;
        niyarinClient.padState.setButtons(niyarinClient.gamePad.getButtons());
        niyarinClient.padState.setAxes(niyarinClient.gamePad.getAxes());
        niyarinClient.padState.update();

        window.setTimeout(loop, 100);
    })();
});