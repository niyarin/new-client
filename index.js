/*
 * index.js
 * 各種ファイルのロードを行う
 * 
 * usage:
 *      buildClient({
 *          yourFile?: yourfile path or [yourfile path1, yourfile path2, ...],
 *          css?: css path or [css1, ccs2, ...]
 *      })
 *      パス　：　カスタマイズ用js, cssの,htmlからの相対パス 
 */


(function(global) {
    global.buildClient = buildClient;

    function buildClient(settings) {
        if(settings == null) settings = {};
        buildDOM();
        buildCss(settings.css);
        buildJs(settings.yourFile);
    }

    function getFilePath() {
        var scripts = document.getElementsByTagName("script");
        for (var i = 0; i < scripts.length; i++) {
            var match = scripts[i].src.match(/(^|.*\/)index\.js$/);
            if (match) return match[1];
        }
        return null;
    }

    function buildDOM() {
        var dom = (function() {
            /*
            <div class="wrapper background">
                <div class="head-wrapper flex-row-group grid" id="head1"></div>
                <div class="main-container flex-row-group" id="ctner">
                    <div id="left-bar" class="side-bar flex-colmn-group">
                        <div class="grid-island grid flex-colmn-list">
                            <p>
                            がれき：<strong style="color:red">X</strong>
                            </p>
                            <p>
                            要救助者：
                            <strong style="color:skyblue">平地X</strong>
                            <strong style="color:lightgreen">家X</strong>
                            </p>
                            <p>
                            機体：
                            <strong style="color:deeppink">X</strong>
                            <strong style="color:darkviolet">X</strong>
                            <strong style="color:yellow">X</strong>
                            <strong style="color:blue">X</strong>
                            </p>
                            <div id="share-board" style="width:190px; height:190px;"></div>
                        </div>
                        <div class="grid-island grid flex-colmn-list" id="custom-area2"></div>
                    </div>
                    <div class="center-bar grid grid-island flex-colmn-group" id="center-bar">
                        <div id="camera-viewer"></div>
                    </div>
                    <div class="side-bar flex-colmn-group" id="right-bar">
                        <div class="grid flex-row-list grid-island" id="custom-area1"></div>
                    </div>
                </div>
                <div class="foot-wrapper flex-row-group  grid" id="footer">
                    <div class="flex-row-list">
                        <div id="message"></div>
                    </div>
                    <div id="pad-state"></div>
                </div>
                <div id="emergency-div" class="flex-colmn-list">
                    <div style="font-size: 6em;">
                        <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> 緊急停止中
                    </div>
                    <div>
                        一切の操作を送信しません
                    </div>
                </div>
            </div>
                    */
        }).toString().match(/\/\*([^]*)\*\//)[1];
        document.write(dom);
    }

    function buildCss(yourCss) {
        var thisFilePath = getFilePath();
        var loadCss = function(path) {
            document.write('<link href="' + path + '" rel="stylesheet" type="text/css">');
        };
        [
            "style/style.css",
            "style/pad-state.css",
            "style/camera-viewer.css",
            "external/font-awesome/css/font-awesome.min.css",
            "external/drawingboard.css",
        ].forEach(function(path) {
            loadCss(thisFilePath + path);
        });
        if(!Array.isArray(yourCss)) yourCss = [yourCss];
        yourCss.forEach(function(css) {
            loadCss(css);
        });
    }

    function buildJs(yourFilePath) {
        var thisFilePath = getFilePath();
        var loadJs = function(path) {
            document.write("<script type='text/javascript' src='" + path + "'></script>");
        };
        [
            "../external/jquery-2.2.4.min.js",
            "../external/jquery.color.min.js",
            "../external/simple-undo.js",
            "../external/drawingboard.js",            
            "communication-drawingboard.js",
            "time-client.js",
            "setting-manager.js",
            "ui/pad-state.js",
            "camera-stream-client.js",
            "ui/camera-viewer.js",
            "ui/head-bar.js",
            "ui/object-tree.js",
            "control-client.js",
            "gamepad/gamepad-mapper.js",
            "gamepad/keyboard-gamepad.js",
            "gamepad/gamepad.js",
            "main.js"
        ].forEach(function(path) {
            loadJs(thisFilePath + "/src/" + path);
        });
        if(!Array.isArray(yourFilePath)) yourFilePath = [yourFilePath];
        yourFilePath.forEach(function(path) {
            loadJs(path);
        });
    }

})((this || 0).self || global);